package exter.substratum.item;

public class ItemDyePowderSmall extends ItemDyePowder
{
  public ItemDyePowderSmall()
  {
    setUnlocalizedName("substratum.dyeSmall");
  }

  @Override
  protected String getName()
  {
    return "dyeSmall";
  }
}
